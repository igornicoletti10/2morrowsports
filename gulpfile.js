const gulp = require('gulp');
const sass = require('gulp-sass');
const nodemon = require('gulp-nodemon');
const browserSync = require('browser-sync').create();

sass.compiler = require('node-sass');

gulp.task('sass', () => {
  return gulp.src('src/public/styles/scss/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('src/public/styles/css'));
});

gulp.task('nodemon', (cb) => {
  let started = false;
  return nodemon({
    script: 'src/server.js',
    ext: 'css, njk, js',
  }).on('start', () => {
    if (!started) {
      started = true;
      cb();
    }
  })
});

gulp.task('browserSync', (done) => {
  return browserSync.init(null, {
    open: false,
    proxy: 'localhost:5000',
    files: [
      'src/app/**/*.*', 
      'src/public/**/*.*'
    ],
    browser: ["google chrome"],
    port: 7000
  }, done)
});

gulp.task('default', gulp.series('sass', 'nodemon', 'browserSync'));
